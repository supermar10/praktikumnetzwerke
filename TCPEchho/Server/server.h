//
// Created by mario on 02.11.18.
//

#ifndef SERVER_SERVER_H
#define SERVER_SERVER_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

int getTCPSocket();

struct sockaddr_in getSocketParameters(uint16_t serverPort);

void bindSocketToAddress(int mainSocket, struct sockaddr_in socketAddress);

void makeSocketListen(int mainSocket);

int tryAcceptConnectionOnSocket(int mainSocket);

void handleReceivedData(int childSocket);

void printError();

#endif //SERVER_SERVER_H
