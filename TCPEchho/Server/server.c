#include "server.h"

const size_t MAXIMUM_MESSAGE_LENGTH = 2000;
const size_t MAX_QUEUE_LENGTH = 5;
int serverRunning = 1;

int main(int argc, char **argv) {
    if (argc < 1) {
        printf("Usage %s port\n", argv[0]);
        exit(1);
    }

    if (argv[1] == NULL) {
        printf("Please supply a port\n");
        exit(1);
    }

    long serverPortTemp = strtol(argv[1], NULL, 10);
    if (serverPortTemp <= 0 || serverPortTemp > 65535) {
        printf("Please supply a valid port\n");
        exit(1);
    }
    uint16_t serverPort = (uint16_t) serverPortTemp;

    int mainSocket = getTCPSocket();

    struct sockaddr_in serverAddress = getSocketParameters(serverPort);

    bindSocketToAddress(mainSocket, serverAddress);
    makeSocketListen(mainSocket);

    while (serverRunning) {
        int childSocket = tryAcceptConnectionOnSocket(mainSocket);
        if (childSocket == -1) {
            printError();
        } else {
            handleReceivedData(childSocket);
            close(childSocket);
        }
    }
}

struct sockaddr_in getSocketParameters(uint16_t serverPort) {
    struct sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(serverPort);
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    return serverAddress;
}

int getTCPSocket() {
    int tcpSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (tcpSocket == -1) {
        printf("Creating socket failed, exiting\n");
        printError();
        exit(1);
    }
    return tcpSocket;
}

void bindSocketToAddress(int mainSocket, struct sockaddr_in socketAddress) {
    if (bind(mainSocket, (struct sockaddr *) &socketAddress, sizeof(socketAddress)) == -1) {
        printError();
        exit(-1);
    }
}

void makeSocketListen(int mainSocket) {
    if (listen(mainSocket, MAX_QUEUE_LENGTH) == -1) {
        printError();
        exit(-1);
    }
}

int tryAcceptConnectionOnSocket(int mainSocket) {
    struct sockaddr clientAddress;
    socklen_t temporarySocketSize = sizeof(clientAddress);
    int childSocket = accept(mainSocket, &clientAddress, &temporarySocketSize);
    return childSocket;
}

void handleReceivedData(int childSocket) {
    char receiveBuffer[MAXIMUM_MESSAGE_LENGTH];
    memset(&receiveBuffer, 0, MAXIMUM_MESSAGE_LENGTH);
    ssize_t lengthReceived = recv(childSocket, receiveBuffer, MAXIMUM_MESSAGE_LENGTH, 0);
    if (lengthReceived == -1) {
        printError();
    } else {
        printf("Client send: %s \n", receiveBuffer);
        fflush(stdout);
        send(childSocket, receiveBuffer, MAXIMUM_MESSAGE_LENGTH, 0);
    }
}

void printError() {
    fprintf(stderr, "Error: %s\n", strerror(errno));
}