//
// Created by mario on 02.11.18.
//

#ifndef CLIENT_CLIENT_H
#define CLIENT_CLIENT_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>

int tryGetTCPSocket();

void writeIpAddress(char *input, struct in_addr *address);

void printError();

struct sockaddr_in
getSocketParameters(char *serverAddress, uint16_t serverPort, struct sockaddr_in *socketParameters);

void parseUserInput(char string[2000]);

#endif //CLIENT_CLIENT_H
