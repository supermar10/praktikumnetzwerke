#include "client.h"

const size_t MAXIMUM_MESSAGE_LENGTH = 2000;

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("Usage %s server port\n", argv[0]);
        exit(1);
    }
    if (argv[1] == NULL) {
        printf("Please supply the host you want to connect to as the first parameter\n");
        exit(1);
    }
    char *serverAddress = argv[1];

    if (argv[2] == NULL) {
        printf("Please supply a port as the first parameter\n");
        exit(1);
    }
    long serverPortTemp = strtol(argv[2], NULL, 10);
    if (serverPortTemp <= 0 || serverPortTemp > 65535) {
        printf("Please supply a valid port as the first parameter\n");
        exit(1);
    }
    uint16_t serverPort = (uint16_t) serverPortTemp;

    int socket = tryGetTCPSocket();
    if (socket == -1) {
        printf("Creating socket failed\n");
        printError();
        exit(1);
    }

    struct sockaddr_in socketParameters;
    socketParameters = getSocketParameters(serverAddress, serverPort, &socketParameters);

    if (connect(socket, (struct sockaddr *) &socketParameters, sizeof(socketParameters)) == -1) {
        printf("Connecting to server failed\n");
        printError();
        exit(1);
    }

    char message[MAXIMUM_MESSAGE_LENGTH];
    parseUserInput(message);

    printf("Sending '%s'\n", message);
    if (send(socket, message, MAXIMUM_MESSAGE_LENGTH, 0) == -1) {
        printf("Sending to server failed\n");
        printError();
        exit(1);
    }

    char receivedMessage[MAXIMUM_MESSAGE_LENGTH];
    if (recv(socket, receivedMessage, MAXIMUM_MESSAGE_LENGTH, 0) == -1) {
        printf("Receiving from server failed\n");
        exit(1);
    }

    printf("Server send: '%s'", receivedMessage);

    close(socket);
    return 0;
}

int tryGetTCPSocket() {
    // The 0 means choose protocol automatically
    return socket(AF_INET, SOCK_STREAM, 0);
}

struct sockaddr_in getSocketParameters(char *serverAddress, uint16_t serverPort,
                                       struct sockaddr_in *socketParameters) {
    writeIpAddress(serverAddress, &(*socketParameters).sin_addr);
    (*socketParameters).sin_family = AF_INET;
    (*socketParameters).sin_port = htons((uint16_t) serverPort);
    return (*socketParameters);
}

void writeIpAddress(char *input, struct in_addr *address) {
    struct in_addr *serverAddressInput = NULL;
    struct hostent *host_info;

    // Check if address is numeric
    if (inet_aton(input, serverAddressInput) > 0) {
        memcpy(address, &serverAddressInput, sizeof(*address));
    } else {
        // Address given is hostname, resolve it
        host_info = gethostbyname(input);
        // Check if hostname was resolved
        if (host_info == NULL) {
            printError();
            exit(errno);
        }
        memcpy(address, host_info->h_addr, (size_t) host_info->h_length);
    }
}

void parseUserInput(char *message) {
    printf("Please enter your message: ");
    scanf("%s", message);
}

void printError() {
    fprintf(stderr, "%s\n", strerror(errno));
}