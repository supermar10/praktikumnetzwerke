#include "server.h"

#define SERVER_NAME "09"

const size_t MAX_QUEUE_LENGTH = 5;
const size_t MAXIMUM_FILE_BUFFER_LENGTH = 1024;
const size_t MAXIMUM_PATH_LENGTH = 10000;
const int MAXIMUM_HEADER_LENGTH = 3000;

const char *NOT_IMPLEMENTED_RESPONSE = "HTTP/1.0 501 Not Implemented\r\n "
                                       "Content-type: text/html\r\n"
                                       "Server: " SERVER_NAME"\r\n\r\n"
                                       "<html><body><b>501</b> Operation not supported</body></html>";

const char *RESSOURCE_NOT_FOUND_RESPONSE = "HTTP/1.0 404 Not found\r\n "
                                           "Content-type: text/html\r\n"
                                           "Server: " SERVER_NAME"\r\n\r\n"
                                           "<html><body><b>404</b> This website is not here, maybe you try google</body></html>";


const char webroot[] = "/var/microwww";


enum httpMethods {
    GET, UNKNOWN
};

int serverRunning = 1;

int main(int argc, char **argv) {
    if (argc < 1) {
        printf("Usage %s port\n", argv[0]);
        exit(1);
    }

    if (argv[1] == NULL) {
        printf("Please supply a port\n");
        exit(1);
    }

    long serverPortTemp = strtol(argv[1], NULL, 10);
    if (serverPortTemp <= 0 || serverPortTemp > 65535) {
        printf("Please supply a valid port\n");
        exit(1);
    }
    uint16_t serverPort = (uint16_t) serverPortTemp;

    // Prevent zombie processes, through ignoring the exit status of the child
    signal(SIGCHLD, SIG_IGN);

    // The main socket which will be used to spawn the child sockets
    int mainSocket = getTCPSocket();

    struct sockaddr_in serverAddress = getSocketParameters(serverPort);

    bindSocketToAddress(mainSocket, serverAddress);
    makeSocketListen(mainSocket);


    while (serverRunning) {
        int childSocket = tryAcceptConnectionOnSocket(mainSocket);
        if (childSocket == -1) {
            printError();
        } else {
            pid_t pid = fork();
            if (pid == 0) {
                handleReceivedData(childSocket);
                // Close child socket as well as main socket, because on fork all file descriptors are copied
                close(childSocket);
                close(mainSocket);
                exit(0);
            } else if (pid == -1) {
                fprintf(stderr, " ERROR: Cannot clone process!\n");
                printError();
                exit(1);
            } else {
                // Close childSocket in parent, so child has full control over this socket
                close(childSocket);
            }
        }
    }
}

struct sockaddr_in getSocketParameters(uint16_t serverPort) {
    struct sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(serverPort);
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    return serverAddress;
}

int getTCPSocket() {
    int tcpSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (tcpSocket == -1) {
        printf("Creating socket failed, exiting\n");
        printError();
        exit(1);
    }
    return tcpSocket;
}

void bindSocketToAddress(int mainSocket, struct sockaddr_in socketAddress) {
    if (bind(mainSocket, (struct sockaddr *) &socketAddress, sizeof(socketAddress)) == -1) {
        printError();
        exit(-1);
    }
}

void makeSocketListen(int mainSocket) {
    if (listen(mainSocket, MAX_QUEUE_LENGTH) == -1) {
        printError();
        exit(-1);
    }
}

int tryAcceptConnectionOnSocket(int mainSocket) {
    struct sockaddr clientAddress;
    socklen_t clientAddressLength = sizeof(clientAddress);
    int childSocket = accept(mainSocket, &clientAddress, &clientAddressLength);
    return childSocket;
}

void handleReceivedData(int childSocket) {
    char receiveBuffer[MAXIMUM_HEADER_LENGTH];
    memset(receiveBuffer, 0, MAXIMUM_HEADER_LENGTH);
    ssize_t lengthReceived = recv(childSocket, receiveBuffer, MAXIMUM_HEADER_LENGTH, 0);
    if (lengthReceived == -1) {
        printError();
    } else {
        printf("Client send: %s \n", receiveBuffer);
        struct HttpRequest httpRequest;
        parseHtmlRequest(receiveBuffer, &httpRequest);

        if (httpRequest.httpMethod == UNKNOWN) {
            send(childSocket, NOT_IMPLEMENTED_RESPONSE, strlen(NOT_IMPLEMENTED_RESPONSE), 0);
        } else if (httpRequest.httpMethod == GET) {
            sendFileToSocket(childSocket, &httpRequest);
        }
    }
}

void parseHtmlRequest(char stringToParse[], struct HttpRequest *httpRequest) {
    printf("Starting parse \n");

    char httpVersion[HTTP_VERSION_LENGTH];
    char requestedFile[MAXIMUM_FILENAME_LENGTH];
    // Buffer to hold the format string
    char format[100];
    sprintf(format, "%%*s %%%ds %%%ds", MAXIMUM_FILENAME_LENGTH - 1, HTTP_VERSION_LENGTH - 1);

    // Splitting the string at the first \n
    char *line = strtok(stringToParse, "\n");
    // Parse line, Http Version, file to load
    sscanf(line, format, requestedFile, httpVersion);

    strcpy(httpRequest->httpVersion, httpVersion);
    strcpy(httpRequest->requestedFile, requestedFile);
    if (startsWith("GET", line) == 1) {
        printf("GET\n");
        httpRequest->httpMethod = GET;
    } else {
        printf("UNKNOWN\n");
        httpRequest->httpMethod = UNKNOWN;
    }
    printf("Parse end\n");
}

void printError() {
    fprintf(stderr, "Error: %s\n", strerror(errno));
}

int startsWith(const char *pre, const char *str) {
    return strncmp(pre, str, strlen(pre)) == 0;
}

void sendFileToSocket(int childSocket, struct HttpRequest *httpRequest) {
    int fileLoaded = 0;
    // Buffer to store a part of the file in
    char data[MAXIMUM_FILE_BUFFER_LENGTH];
    char fullPath[MAXIMUM_PATH_LENGTH];
    sprintf(fullPath, "%s%s", webroot, (*httpRequest).requestedFile);
    // Check for .. in path, if so respond with 404
    if (checkForTreeTraversal(fullPath) == -1) {
        fileLoaded = -1;
    } else {
        FILE *fp = fopen(fullPath, "r");
        if (fp == NULL) {
            fprintf(stderr, "Can't open file %s!\n", fullPath);
            fileLoaded = -1;
        } else {
            char httpResponse[MAXIMUM_HEADER_LENGTH];
            constructHttpResponse(*httpRequest, httpResponse, 200, "OK");
            printf("Sending: \n%s\n", httpResponse);
            send(childSocket, httpResponse, strlen(httpResponse), 0);

            // Send file requested
            do {
                size_t bytesRead = readFileChunk(data, fp);
                fileLoaded = (int) bytesRead;
                send(childSocket, data, bytesRead, 0);
            } while (fileLoaded == (int) MAXIMUM_FILE_BUFFER_LENGTH);
            fclose(fp);
        }
    }
    // File not loadable send 404
    if (fileLoaded == -1) {
        send(childSocket, RESSOURCE_NOT_FOUND_RESPONSE, strlen(RESSOURCE_NOT_FOUND_RESPONSE), 0);
    }
    printf("Sending finished");
}

size_t readFileChunk(char *data, FILE *fp) {
    size_t bytesRead = fread(data, sizeof(char), MAXIMUM_FILE_BUFFER_LENGTH, fp);
    return bytesRead;
}

void constructHttpResponse(struct HttpRequest request, char response[], int httpStatusCode,
                           char *httpStatusText) {
    sprintf(response, "%s %i %s \r\nContent-Type: text/html\r\nServer: "SERVER_NAME"\r\n\r\n", request.httpVersion,
            httpStatusCode, httpStatusText);
}

int checkForTreeTraversal(char *path) {
    regex_t regex;
    int regexResult;

    // Prepare the regex, escape the backslash and the point
    if (regcomp(&regex, "\\.\\.", 0)) {
        fprintf(stderr, "Could not compile regex\n");
        return -1;
    }
    // Execute the regex
    regexResult = regexec(&regex, path, 0, NULL, 0);
    // and compare the result
    if (regexResult == REG_NOMATCH) {
        printf("No traversal found\n");
        regfree(&regex);
        return 0;
    } else {
        printf("Traversal found or error\n");
        regfree(&regex);
        return -1;
    }
}