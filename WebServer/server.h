#ifndef WEBSERVER_WEBSERVER_H
#define WEBSERVER_WEBSERVER_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <regex.h>
#include <signal.h>
#include <unistd.h>
#include "HttpRequest.h"

int getTCPSocket();

struct sockaddr_in getSocketParameters(uint16_t serverPort);

void bindSocketToAddress(int mainSocket, struct sockaddr_in socketAddress);

void makeSocketListen(int mainSocket);

int tryAcceptConnectionOnSocket(int mainSocket);

void handleReceivedData(int childSocket);

void printError();

void parseHtmlRequest(char stringToParse[], struct HttpRequest *httpRequest);

int startsWith(const char *pre, const char *str);

void sendFileToSocket(int childSocket, struct HttpRequest *httpRequest);

size_t readFileChunk(char *data, FILE *fp);

void constructHttpResponse(struct HttpRequest request, char response[], int httpStatusCode,
                           char *httpStatusText);

int checkForTreeTraversal(char *path);

#endif //WEBSERVER_WEBSERVER_H
