#ifndef SERVER_HTTPREQUEST_H
#define SERVER_HTTPREQUEST_H

#define HTTP_VERSION_LENGTH 9
#define MAXIMUM_FILENAME_LENGTH 1000

struct HttpRequest {
    int httpMethod;
    char requestedFile[MAXIMUM_FILENAME_LENGTH];
    char httpVersion[HTTP_VERSION_LENGTH];
};

#endif //SERVER_HTTPREQUEST_H
